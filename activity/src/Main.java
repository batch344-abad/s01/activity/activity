import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
    String firstName;
    String lastName;
    double firstSubject;
    double secondSubject;
    double thirdSubject;

        Scanner myAct = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = myAct.nextLine();

        System.out.println("Last Name:");
        lastName = myAct.nextLine();

        System.out.println("First Subject Grade:");
        firstSubject = myAct.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubject = myAct.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubject = myAct.nextDouble();

        double average = (firstSubject + secondSubject + thirdSubject) / 3.0;

        System.out.println("Good day, " + firstName + " " + lastName);

        System.out.println("Your grade average is: " + average);

    }
}